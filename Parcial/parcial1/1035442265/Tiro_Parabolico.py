import numpy as np
import matplotlib.pyplot as plt
import math

class TiroParabolico():
    def __init__(self,vo,xo,ho,radtheta,g,ax):

        #Atributos
        self.vo = vo
        self.radtheta =radtheta
        self.xo=xo
        self.ho=ho
        self.g=g
        self.ax=ax

    def VoX(self):
        vox = self.vo * round(math.cos(self.radtheta),3)
        return  vox
    
    def VoY(self):
        voy = self.vo * round(math.sin(self.radtheta),3)
        return  voy
    
    def TiempoDeVuelo(self):
        try:
            tv = (- self.VoY() - np.sqrt(self.VoY()**2 - 2* self.g * self.ho))/self.g
            return tv 
        except:
            return "Error en el calculo de tiempo de vuelo, revisar parametros"
        
    def aTime(self):
        a_time = np.arange(0,self.TiempoDeVuelo(),0.01)
        return a_time
    
    def VelocidaEnX(self):
        VX = self.VoX() + self.ax * self.aTime()
        return VX
    def VelocidaEnY(self):
        VY = self.VoY() + self.g * self.aTime()
        return VY
    
    def AlcanceMax(self):
        Xmax = self.xo + self.VoX() * self.TiempoDeVuelo() + (1/2) * self.ax * self.TiempoDeVuelo()**2
        return Xmax
    
    def TYmax(self):
        ty = -(self.VoY()/self.g)
        return ty
    
    def AlturaMax(self):
        ymax = self.ho + self.VoY() * self.TYmax() + (1/2)* self.g * self.TYmax()**2
        return ymax
    

    def posX(self):
        posx = [self.xo + self.VoX() * i + (1/2)* self.ax * i**2 for i in self.aTime()]
        return  posx

    def posY(self):
        posy = [self.ho + self.VoY() * i + (1/2)* self.g * i**2 for i in self.aTime()]
        return  posy
    
    def figMp(self):

        plt.figure(figsize=[10,8])
        plt.plot(self.posX(),self.posY())
        plt.savefig("trayectoria.png")

