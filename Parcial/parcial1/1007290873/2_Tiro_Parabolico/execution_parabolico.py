import numpy as np
from TiroParabolico import tiroParabolico, tiroParabolicoPlanoInclinado

if __name__ == "__main__":

    # Definimos los parametros de entrada para instanciar la clase tiroParabolico (unidades en mks)

    velocidad_inicial = 30
    angulo = 45
    posicion_inicial_x = 0
    altura_inicial = 20
    aceleracion_y = -9.81
    aceleracion_x = -5

    # Con estos se crea el objeto tiro_parabolico 

    tiro_parabolico = tiroParabolico(velocidad_inicial, angulo, posicion_inicial_x, altura_inicial, aceleracion_y, aceleracion_x)

    # Prueba imprimiendo algunos atributos y métodos

    print("-Tiro Parabólico:\n")

    print("Velocidad inicial en x y y: ({}, {}) m/s".format(tiro_parabolico.vel_inicial_x, tiro_parabolico.vel_inicial_y))

    print("Altura máxima: {} m".format(tiro_parabolico.AlturaMax()))

    print("Alcance máximo: {} m".format(tiro_parabolico.AlcanceMax()))

    print("Tiempo de vuelo: {} s".format(tiro_parabolico.TiempoVuelo()))

    # Gráfica de este movimiento

    tiro_parabolico.GraficaMovimiento() 

    # Parametros de entrada para instanciar la clase tiroParabolicoPlanoInclinado (unidades en mks)

    velocidad_inicial_inc = 30
    angulo_inc = 55
    posicion_inicial_x_inc = 0
    altura_inicial_inc = 0
    aceleracion_y_inc = -9.81
    aceleracion_x_inc = -5
    angulo_inclinado_inc = 30

    # Se crea el objeto tiro_parabolico_plano_inclinado 

    tiro_parabolico_plano_inclinado = tiroParabolicoPlanoInclinado(velocidad_inicial_inc, angulo_inc, posicion_inicial_x_inc, altura_inicial_inc, aceleracion_y_inc, aceleracion_x_inc, angulo_inclinado_inc)

    print("\n-Herencia (Plano Inclinado):\n")

    print("Altura máxima con respecto al plano inclinado: {} m".format(tiro_parabolico_plano_inclinado.AlturaMax()))

    print("Alcance máximo en el plano inclinado: {} m".format(tiro_parabolico_plano_inclinado.AlcanceMax()))

    print("Tiempo de vuelo: {} s".format(tiro_parabolico_plano_inclinado.TiempoVuelo()))

    # Gráfica del movimiento

    tiro_parabolico_plano_inclinado.GraficaMovimiento()