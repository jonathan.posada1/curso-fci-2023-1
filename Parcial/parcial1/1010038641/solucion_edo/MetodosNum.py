import numpy as np
import sympy as sym
from matplotlib import pyplot as plt
from scipy.integrate import odeint


'''Clase que contiene métodos para resolver ecuaciones diferenciales:
la clase MetodosNum contiene la implementación de Euler para una función general f(x, y),
en un intervalo [a, b], con una condición inifical f(x0, y) = y0 y un número de pasos n'''

'''La solución analítica de la ecuación es hecha mediante scipy odeint'''

class MetodosNum():
    # constructor
    def __init__(self, a, b, y0, n, f, point=False):
        # definimos los atributos
        self.a = a
        self.b = b
        self.y0 = y0
        self.n = n
        self.f = f
        self.y = [] # listas para llenar con los valores de  y

    # Métodos

    # divisiones del intervalo
    def h(self):
        return (self.b - self.a) / self.n

    # rango de x
    def X(self):
        return np.arange(self.a, self.b, self.h())

    # implementamos el método de euler
    def euler(self):
        self.y = [self.y0]

        for i in range(self.n-1):
            self.y.append(self.y[i] + self.h() * self.f(self.y[i], self.X()[i]))
        return self.X(), self.y

    # ====================================================================================
    #Solución análitica mediante Scipy odeint
    def sol_scipy(self):
        y_sc = odeint(self.f, self.y0, np.linspace(0, 1, self.n))
        return y_sc

    # ====================================================================================

    # ====================================================================================
    # graficamos la solución obtenida por método de euler y por el método analítico
    def graficar(self):
        plt.title('Solución análitica y númerica de la ecuación $y\'(x) = e^{x}$')
        plt.plot(self.X(), self.euler()[1], color='red', label = 'Euler', linewidth=5)
        plt.plot(self.X(), self.sol_scipy(), color = 'blue', label = 'Analítica')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.legend()
        plt.show()
        return
    # ====================================================================================
    # ====================================================================================

''' Para implementar la solución del péndulo simple mediante euler se debe hacer un sútil cambio en el algoritmo 
anterior, si bien se podría usar herencia(la cual ya usamos en el punto 1 y 2 del examen) , por facilidad definamos
 una nueva clase independiente'''

# Solución por método de Euler para el péndulo simple en pequeñas oscilaciones

'''Esta clase recibe las condiciones iniciales de ángulo y velocidad angular del péndulo, el tiempo de integración t
 con un paso dt, y recibe dos funciones, f1 y f2, que corresponden a las EDO de primer orden que se obtienen mediante 
 la reducción de orden (ver pdf). Adicionalmente, recibe como parametro eq, el sistema de ecuaciones evaluado en 
 el vector de condiciones iniciales para la implementación de la solución analítica'''

class PenduloEuler():
    def __init__(self, theta0, w0, t, dt, f1, f2, eq):
        print('inicializando clase péndulo simple')
        self.theta0 = theta0
        self.w0 = w0
        self.t = t
        self.dt = dt
        self.f1 = f1
        self.f2 = f2
        self.theta = []
        self.w = []
        self.eq = eq

     # intervalo temporal
    def tiempo(self):
        return np.arange(0, self.t, self.dt)

    # método de Euler para el péndulo simple
    def euler(self):
        self.theta = [self.theta0]
        self.w = [self.w0]

        for i in range(len(self.tiempo())-1):
            self.w.append(self.w[i] + self.dt * self.f2(self.theta[i]))
            self.theta.append(self.theta[i] + self.dt * self.f1(self.w[i+1]))


        return self.theta, self.w

    # ====================================================================================
    '''Adicionalmente tenemos un método que resuelve la ecuación del péndulo simple de forma general
    (sin evalular las condiciones iniciales) mediante sympy'''
    def sol_sympy(self):
        # definimos los simbolos necesarios
        t = sym.Symbol('t')
        g = sym.Symbol('g')
        l = sym.Symbol('l')
        w = sym.Symbol('omega')
        theta = sym.Function('theta')

        # función simbólica
        f_sym = -w ** 2 * theta(t)

        # resolvemos la ecuación diferencial de forma general
        sol_sym = sym.dsolve(sym.diff(theta(t), t, t) - f_sym)

        return sol_sym

    # ====================================================================================
    '''Implementación analítica (númerica) mediante odeint'''

    # vector con las condiciones iniciales
    def y0(self):
        return [self.theta0, self.w0]

    # llamamos la solución
    def sol_num(self):
        y_num = odeint(self.eq, self.y0(), np.arange(0, self.t, 0.01))
        return y_num

    # ====================================================================================

    # Finalmente graficamos
    def graficar_theta(self):
        plt.title('Posición angular para el péndulo simple')
        plt.plot(np.arange(0, self.t, 0.01), self.sol_num()[:,0], color = 'red', label = 'solución analítica')
        plt.plot(self.tiempo(), self.euler()[0], label = 'solución euler')
        plt.xlabel('t')
        plt.ylabel('$θ(t)$')
        plt.legend()
        plt.show()
        return

    def graficar_w(self):
        plt.title('Velocidad angular para el péndulo simple')
        plt.plot(np.arange(0, self.t, 0.01), self.sol_num()[:,1], color = 'red', label = 'solución analítica')
        plt.plot(self.tiempo(), self.euler()[1], label = 'solución euler')
        plt.xlabel('t')
        plt.ylabel('$\omega(t)$')
        plt.legend()
        plt.show()
        return