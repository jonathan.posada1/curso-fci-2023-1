from pendulo import PenduloBalistico, PenduloHijo




if __name__ == "__main__":

#parametros para modeloar el pendulo balistico
    theta = 140 #angulo de defleccion
    g = 9.8 #aceleracion gravitacional
    l = 10  # longitud de la cuerda
    m = 0.1  # masa bala
    M = 10  # masa pendulo
    v0 = 400  # velocidad inicial bala
    
    #******se hacen instancias de las clases con sus respectivos parametros**********

    miPendulo = PenduloBalistico(l, m, M, v0, g)
    miPendulo2 = PenduloHijo(l, m, M, v0, g, theta)

    #********se llaman los metodos que imprimen por consola la informacion fisica*****
    miPendulo.calculate_deflection_angle()
    miPendulo.calculate_block_bullet_velocity()
    miPendulo2.velPend()
    miPendulo2.velBal()
    miPendulo2.calcularPeriodo()
    miPendulo2.graficar()
   



   

    