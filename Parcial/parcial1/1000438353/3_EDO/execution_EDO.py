from EDO import SolEDO
import numpy as np
import sympy as sp


if __name__=="__main__":


    #Configuración
    a=0                 # límite inferior intervalo de integración
    b=1                 # límite superior intervalo de integración
    x0=0                # condición inicial x
    y0=-1               # condición inicial y
    n=10                # número de pasos de integración para el método de Euler
    
    # dy/dx = Df
    #Df=lambda x,y: -2*x+y*x
    Df=lambda x,y: np.exp(-x)

    #Llamar clase
    sol=SolEDO(a,b,Df,x0,y0,n)
    
    sol.Plot("SolEDO.png")