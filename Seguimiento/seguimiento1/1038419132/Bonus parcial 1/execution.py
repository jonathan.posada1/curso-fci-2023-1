from Parabolic import parabo
from Parabolic import paraboa
import numpy as np
import matplotlib.pyplot as plt

"""
NOTA:
4.5

-No funciona para g=0
-No guarda las trayectorias

"""

if __name__=="__main__":

    
    g = 9.89  #parametros iniciales, gravedad, posini, veloini angulo inicial
    yo = 0
    vo = 20
    grad = 30
    a = 13 #aceleración encontra del movimiento
    b = 2*vo*np.sin(np.pi/180*grad) /g  #tiempo de vuelo
    t = np.arange(0,b,0.001) #tiempo de vuelo en array

    p1 = parabo(t,g,yo,vo,grad) #heredo la clase parabola a la parabola1 (p1)
    p2 = paraboa(t,g,yo,vo,grad,a)#heredo la clase parabola a la parabola2 (p2)
    xp1 = p1.motionx()
    yp1 = p1.motiony() #llamo a los metodos
    xp2 = p2.motionnx()
    yp2 = p2.motiony() #llamo a los metodos
    
    plt.plot(xp1,yp1,'r',label = 'P con resis =  -{} m/s^2'.format(a)) #grafico
    plt.plot(xp2,yp2,'b',label = 'P sin resis') #grafico
    plt.legend()
    plt.title('Tiro parabólico')
    plt.xlabel('Distancia [m]')
    plt.ylabel('Altura [m]')
    plt.show()













