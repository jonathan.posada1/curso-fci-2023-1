from particula_material import Material, Particle, simulate_particle
import matplotlib.pyplot as plt
# Buen trabajo
# 5


if __name__ == "__main__":
    density = 1.0  # densidad del material en g/cm3
    energy_loss_probabilities = {
        0.1: 0.1,
        0.5: 0.4,
        1.0: 0.3,
        5.0: 0.2
    }  # probabilidades de pérdida de energía en cada célula
    material = Material(density, energy_loss_probabilities)
    particle = Particle(100.0)  # energía inicial de la partícula
    num_cells = 100  # número de células que atraviesa la partícula

    energies = simulate_particle(particle, material, num_cells)

    plt.plot(range(num_cells), energies)
    plt.xlabel('Célula')
    plt.ylabel('Energía (MeV)')
    plt.title('Evolución de la energía de una partícula que atraviesa un material')
    plt.savefig("E_particula.png")
    plt.show()

