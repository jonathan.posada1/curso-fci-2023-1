from ising import IsingModel


if __name__ == '__main__':
    ising_model = IsingModel(N=20, J=1, H=0)
    ising_model.plot_energies(T_min=1, T_max=4, num_steps=100)
    ising_model.plot_magnetization(T_min=1, T_max=4, num_steps=100)
    ising_model.plot_specific_heat(T_min=1, T_max=4, num_steps=100)
