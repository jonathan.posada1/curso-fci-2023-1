from MonteCarloIntegracion import monteCarloIntegracion
# BUEN TRABAJO
# 5.0

if __name__ == "__main__":
    # Número máximo de pasos para la integración con MC
    N = 10000

    # Se instancia la clase para este valor particular de N
    integracion_con_MC = monteCarloIntegracion(N)

    print("Valor de la integral con MC, para N = {}: {}".format(N, integracion_con_MC.area_MC_result(N)))
    print("Valor analítico de la integral: {}".format(integracion_con_MC.area_analytical_result()))

    # Gráfica con los resultados
    integracion_con_MC.graph_results()