import numpy as np
import matplotlib.pylab as plt
#Buen trabajo
#5.0
class normal():
    def __init__(self, n, h, s, u):
        self.n = n
        self.h = h
        self.s = s
        self.u = u

    #función de distribución gaussiana
    def gaussiana(self, t): 
        return 1/np.sqrt(2*np.pi*self.s**2) * np.exp(-(t - self.u)**2./2.)
    

    def distribucion(self):
        t = np.linspace(-5, 5, 1000)   # rango en x 
        r = -5 + np.random.rand(self.n) * 8 # datos aleatorios en x entre -5 y 5
        y = self.h  * np.random.rand(self.n) # generar puntos aleatorios en y entre [0, h]

        # en la lista vacía gurdamos los puntos que estén por debajo de f(t)
        hl = [] 
        for i in range(self.n):
            if y[i] < self.gaussiana(r[i]):
                hl.append(r[i])

        # graficamos
        plt.figure(figsize=(10,8))                                                         
        plt.hist(hl, bins=100, density=True)
        plt.plot(t,self.gaussiana(t))
        plt.savefig('try.jpg')
        plt.savefig('distribucion.jpg')
        plt.show()
        
        #return 
